# -*- coding: utf-8 -*-
###############################################################################
# PySword - A native Python reader of the SWORD Project Bible Modules         #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2016 Various developers:                                 #
# Kenneth Arnold, Joshua Gross, Ryan Hiebert, Matthew Wardrop, Tomas Groth    #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 51  #
# Franklin St, Fifth Floor, Boston, MA 02110-1301 USA                         #
###############################################################################

from unittest import TestCase
try:
    from unittest.mock import MagicMock
except:
    from mock import MagicMock

from pysword.books import BibleStructure


class TestBibleStructure(TestCase):
    def test_init_invalid_versification(self):
        """
        Test that BibleStructure raises an exception on invalid versification
        """
        # GIVEN: An invalid versification
        versification = u'does_not_exists'

        # WHEN: Creating a new BibleStructure
        # THEN: An ValueError exception should be raised
        self.assertRaises(ValueError, BibleStructure,  versification)

    def test_ref_to_indicies(self):
        """
        Test that ref_to_indicies can handle unicode and non-unicode input, only makes sense for python2
        """
        # GIVEN: A bible structure with mocked find_book
        bible_structure = BibleStructure(u'default')
        mocked_find_book = MagicMock()
        mocked_find_book.return_value = ('mocked_testament', MagicMock())
        bible_structure.find_book = mocked_find_book
        bible_structure._book_offset = MagicMock()

        # WHEN: Calling ref_to_indicies with both str and unicode input
        bible_structure.ref_to_indicies('test')
        bible_structure.ref_to_indicies(u'test')

        # THEN: The mocked find_book should have been called twice
        self.assertEqual(mocked_find_book.call_count, 2, u'The mocked find_book should have been called twice')
