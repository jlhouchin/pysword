# -*- coding: utf-8 -*-
###############################################################################
# PySword - A native Python reader of the SWORD Project Bible Modules         #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2016 Various developers:                                 #
# Kenneth Arnold, Joshua Gross, Ryan Hiebert, Matthew Wardrop, Tomas Groth    #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 51  #
# Franklin St, Fifth Floor, Boston, MA 02110-1301 USA                         #
###############################################################################

import os
from unittest import TestCase
try:
    from unittest.mock import patch
except:
    from mock import patch

from pysword.modules import SwordModules

TEST_RESOURCE_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), u'resources')


class TestSwordModules(TestCase):
    def test_parse_modules_folder(self):
        """
        Test that conf file from a folder can be parsed.
        """
        # GIVEN: A SwordModules object using a folder for input
        modules = SwordModules(TEST_RESOURCE_FOLDER)

        # WHEN: parsing the modules conf files
        mods_metadata = modules.parse_modules()

        # THEN: Modules should be detectable and information extractable
        self.assertTrue(all(x in [u'ChiPinyin', u'FinPR', u'BSV'] for x in mods_metadata.keys()),
                        u'Some expected bibles were note detected')
        # Depending on the operating system, the handling of non-utf8 encoded conf-files is different
        if os.name == u'nt':
            self.assertEqual(mods_metadata['FinPR']['description'], u'Finnish Pyhä Raamattu (1933/1938)',
                             u'Could not extract "description" for "FinPR"')
        else:
            self.assertEqual(mods_metadata['FinPR']['description'], u'Finnish Pyh\ufffd Raamattu (1933/1938)',
                             u'Could not extract "description" for "FinPR"')
        self.assertEqual(mods_metadata['BSV']['description'], u'The Bond Slave Version Bible',
                         u'Could not extract "description" for "BSV"')

    def test_parse_modules_zip(self):
        """
        Test that conf file from a zip can be parsed.
        """
        # GIVEN: A SwordModules object using a folder for input
        modules = SwordModules(os.path.join(TEST_RESOURCE_FOLDER, u'FinPR.zip'))

        # WHEN: parsing the modules conf files
        mods_metadata = modules.parse_modules()

        # THEN: Modules should be detectable and information extractable
        self.assertEqual(1, len(mods_metadata.keys()), u'There should be only 1  module in a zip.')
        self.assertIn(u'FinPR', mods_metadata.keys(), u'FinPR should be available')
        # Depending on the operating system, the handling of non-utf8 encoded conf-files is different
        if os.name == u'nt':
            self.assertEqual(mods_metadata['FinPR']['description'], u'Finnish Pyhä Raamattu (1933/1938)',
                             u'Could not extract "description" for "FinPR"')
        else:
            self.assertEqual(mods_metadata['FinPR']['description'], u'Finnish Pyh\ufffd Raamattu (1933/1938)',
                             u'Could not extract "description" for "FinPR"')

    @patch(u'pysword.modules.SwordBible')
    def test_get_bible_from_module(self, mocked_sword_bible):
        """
        Test that the assigning of default values works.
        """
        # GIVEN: A SwordModules object
        modules = SwordModules(u'test_sword_path')
        modules._modules = {u'test_key': {u'datapath': u'test_path', u'moddrv': u'test_mod_type'}}

        # WHEN: Requesting a bible from a module
        bible = modules.get_bible_from_module(u'test_key')

        # THEN: It should succeed
        # Check that the returned mock bible has created with default values
        self.assertIsNotNone(bible, u'Returned bible should not be None')
        mocked_sword_bible.assert_called_with(os.path.join(u'test_sword_path', u'test_path'),
                                              u'test_mod_type', u'default', None, None)
