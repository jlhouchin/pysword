# -*- coding: utf-8 -*-
###############################################################################
# PySword - A native Python reader of the SWORD Project Bible Modules         #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2016 Various developers:                                 #
# Kenneth Arnold, Joshua Gross, Ryan Hiebert, Matthew Wardrop, Tomas Groth    #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 51  #
# Franklin St, Fifth Floor, Boston, MA 02110-1301 USA                         #
###############################################################################

import os
from unittest import TestCase
try:
    from unittest.mock import patch
except:
    from mock import patch

from pysword.bible import SwordModuleType, SwordBible

TEST_RESOURCE_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), u'resources')


class TestBookStructure(TestCase):

    @patch('pysword.modules.SwordBible._get_ztext_files')
    def test_init_only_one_testament(self, mocked_get_ztext_files):
        """
        Test that when loading a bible with only one testament, only that testament is available
        """
        # GIVEN: A mocked _get_ztext_files method in SwordBible
        mocked_get_ztext_files.side_effect = [True, IOError('Not good')]

        # WHEN: Creating a SwordBible
        bible = SwordBible(u'test_path', SwordModuleType.ZTEXT, u'default', 'utf-8', u'OSIS')

        # THEN: Only 'nt' should have been loaded
        testaments = list(bible._files.keys())
        self.assertListEqual(testaments, [u'ot'],  u'Only "ot" files should be available')

    def test_init(self):
        """
        Test that the init loading of bible works
        """
        # GIVEN: A bible
        path = os.path.join(TEST_RESOURCE_FOLDER, u'modules', u'texts', u'ztext', u'finpr')

        # WHEN: Loading the bible
        bible = SwordBible(path)

        # THEN: It should load
        self.assertIsNotNone(bible, u'The bible should have loaded')
